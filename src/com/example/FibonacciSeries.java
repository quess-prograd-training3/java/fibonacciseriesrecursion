package com.example;

public class FibonacciSeries {
    int firstNumber;
    int secondNumber;
    int thirdNumber;

    public FibonacciSeries(int firstNumber, int secondNumber, int thirdNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.thirdNumber = thirdNumber;
    }

    public void FindSeries(int number){
        if(number>0){
            System.out.print(firstNumber+" ");
            thirdNumber=firstNumber+secondNumber;
            firstNumber=secondNumber;
            secondNumber=thirdNumber;
            FindSeries(number-1);
        }
    }
}
